import { createStore, combineReducers } from 'redux';
import appReducer from './reducers/app';

const rootReducer = combineReducers({
  app: appReducer
})

const store = createStore(rootReducer);

export default store