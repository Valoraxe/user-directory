export const getUsers = (users) => ({
    type: "GET_USERS",
    users
});

export const login = (token) => ({
    type: "LOG_IN",
    token
});

export const logout = () => ({
    type: "LOG_OUT"
});

export const continueSession = () => ({
    type: "CONTINUE_SESSION"
});