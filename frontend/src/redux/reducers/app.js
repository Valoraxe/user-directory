export default (state = {loggedIn: false, users: []}, action) => {
    switch (action.type) {
        case "GET_USERS":
            return {
                ...state, users: action.users
            };
        case "LOG_IN":
            localStorage.setItem("MoserToken", action.token);
            return {
                ...state, loggedIn: true
            };
        case "LOG_OUT":
            localStorage.removeItem("MoserToken");
            return {
                ...state, loggedIn: false
            };
        case "CONTINUE_SESSION":
            return {
                ...state, loggedIn: true
            }
        default:
            return state;
    }
}