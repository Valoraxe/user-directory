import React from 'react'
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import Store from './redux/store';
import { continueSession } from './redux/actions/actions'
import './index.css'
import MainRouter, { history } from './routers/mainRouter';

const App = (
    <Provider store={Store}>
        <MainRouter/>
    </Provider>
);

const renderApp = () => ReactDOM.render(App, document.getElementById('root'));

const token = localStorage.getItem("MoserToken");

if (token) {
    Store.dispatch(continueSession());
    renderApp();
    if (history.location.pathname === "/") {
        history.push("/home");
    }
} else {
    renderApp();
    history.push("/");
}
