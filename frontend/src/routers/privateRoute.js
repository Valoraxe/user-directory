import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({component: Component}) => {
    const loggedIn = useSelector(state => state.app.loggedIn);
  
    return (
        <Route component={(props) => (
            loggedIn ? (
                <Component {...props}/> 
              ) : (
                <Redirect to="/"/>
              )  
        )}/>
    )
}

export default PrivateRoute
