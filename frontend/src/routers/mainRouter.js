import React from 'react';
import PrivateRoute from './privateRoute';
import PublicRoute from './publicRoute';
import LoginPage from '../components/loginPage';
import HomePage from '../components/homePage';
import CreatePage from '../components/createPage';
import EditPage from '../components/editPage';
import { createBrowserHistory as createHistory } from 'history';
import { Router, Switch } from 'react-router-dom';

export const history = createHistory()

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true}/>
        <PrivateRoute path="/home" component={HomePage}/>
        <PrivateRoute path="/create" component={CreatePage}/>
        <PrivateRoute path="/edit/:id" component={EditPage}/>
      </Switch>
    </div>
  </Router>
)

export default AppRouter
