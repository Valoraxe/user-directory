import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";

const Form = ({ data, submitFunction, serverError }) => {
    const history = useHistory();
    const [firstName, setFirstname] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [phone, setPhone] = useState("");
    const [title, setTitle] = useState("");

    useEffect(() => {
        if (data) {
            setFirstname(data.firstName);
            setLastName(data.lastName);
            setEmail(data.email);
            setPassword(data.password);
            setPhone(data.phone);
            setTitle(data.title);
        }
    }, [data])

    const checkBeforeSubmit = (e) => {
        e.preventDefault();

        const myData = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            phone: phone,
            title: title
        }

        submitFunction(myData);
    }

    return (
        <div className="box-layout_box">
            
            <form onSubmit={checkBeforeSubmit}>
                <input value={firstName || ""} className='text-input'
                    placeholder="First Name" onChange={(e) => setFirstname(e.target.value)}/>
                <input value={lastName || ""} className='text-input'
                    placeholder="Last Name" onChange={(e) => setLastName(e.target.value)}/>
                <input value={email || ""} className='text-input'
                    placeholder="E-mail Address" onChange={(e) => setEmail(e.target.value)}/>
                <input value={password || ""} className='text-input' type="password"
                    placeholder="Password" onChange={(e) => setPassword(e.target.value)}/>
                <input value={phone || ""} className='text-input'
                    placeholder="Phone: 123-456-7890" onChange={(e) => setPhone(e.target.value)}/>
                <input value={title || ""} className='text-input'
                    placeholder="Title" onChange={(e) => setTitle(e.target.value)}/>
                <button className="button">Save Data</button>
            </form>
            <button className="button" onClick={() => history.goBack()}>Cancel</button>
            {serverError && 
                <p className="error">Your data has not been sent! Please check your information and try again.</p>
            }
        </div>
        
    );
}

export default Form