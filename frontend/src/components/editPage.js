import React, {useState, useEffect} from "react";
import Form from './form';
import { useLocation, useHistory } from "react-router";

const EditPage = () => {
    const [user, setUser] = useState({});
    const [serverError, setServerError] = useState(false);
    const history = useHistory();
    const pathName = useLocation().pathname;

    useEffect(() => {
        apiGetUser().then(res => {
            setUser(res);
        }).catch(err => {
            console.log(err);
        });
    }, [])

    const updateUser = (myData) => {
        setServerError(false);
        let patchedData = {};
        if (user.firstName !== myData.firstName) { patchedData.firstName = myData.firstName }
        if (user.lastName !== myData.lastName) { patchedData.lastName = myData.lastName }
        if (user.email !== myData.email) { patchedData.email = myData.email }
        if (user.password !== myData.password) { patchedData.password = myData.password }
        if (user.phone !== myData.phone) { patchedData.phone = myData.phone }
        if (user.title !== myData.title) { patchedData.title = myData.title }
        apiPatchUser(patchedData).then(res => {
            if(res.status === 200) {
                history.push("/");
            } else {
                setServerError(true);
            }
        }).catch(err => {
            console.log(err);
        })
    }

    const apiGetUser = async () => {
        let userId = pathName.slice(6);
        let response = await fetch(`/users/${userId}`);
        let body = await response.json();
    
        return body
    };

    const apiPatchUser = async (patchedData) => {
        let userId = user.id;
        let response = await fetch(`/users/${userId}`, {
            method: 'PATCH', 
            body: JSON.stringify(patchedData),
            headers: { "Content-type" : "application/json; charset=UTF-8"} 
        });

        return response
    }

    return (
        <>
            {user && <div className="box-layout">
                <Form data={user} submitFunction={updateUser} serverError={serverError}/>    
            </div>}
        </>
    );
}

export default EditPage