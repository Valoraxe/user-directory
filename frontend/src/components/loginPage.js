import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { login } from '../redux/actions/actions';

const LoginPage = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loginError, setLoginError] = useState(false);

    const dispatch = useDispatch();

    const performLogin = (e) => {
        e.preventDefault();
        setLoginError(false);

        const creditentials = { email: email, password: password };
        apiPostLogin(creditentials).then(res => {
            if (res.statusCode) {
                setLoginError(true);
            } else {
                dispatch(login(res.token))
            }
        }).catch(err => {
            console.log(err);
        })
    }

    const apiPostLogin = async (creditentials) => {
        let response = await fetch(`/login`, {
            method: 'POST', 
            body: JSON.stringify(creditentials),
            headers: { "Content-type" : "application/json; charset=UTF-8"} 
        });
        let body = await response.json()

        return body
    }

    return (
        <div className="box-layout">
            <div className="box-layout_box">
                <form onSubmit={performLogin}>
                    <input value={email} className="text-input"
                        placeholder="E-mail" onChange={(e) => setEmail(e.target.value)}/>
                    <input value={password} className="text-input" type="password" 
                        placeholder="Password" onChange={(e) => setPassword(e.target.value)}/>
                    <button className="button">Sign In</button>
                </form>
                {loginError && <p className="error">Email/Password combination invalid! Please try again.</p>}
            </div>
        </div>
    )
}

export default LoginPage
