import React from "react";
import { Link } from 'react-router-dom';

const User = ({ user }) => {
    return (
        <Link to={`/edit/${user.id}`} className="list-item">
            <div>
                <h3 className="list-item_title">{`${user.firstName} ${user.lastName}`}</h3>
                <span>{user.email}</span>
                
            </div>

            <div>
                <span>{user.id}</span>
            </div>
        </Link>
    );
}

export default User