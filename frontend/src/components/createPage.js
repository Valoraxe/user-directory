import React, { useState } from "react";
import Form from './form';
import { useHistory } from "react-router";

const CreatePage = () => {
    const [serverError, setServerError] = useState(false);
    const history = useHistory();

    const createUser = (myData) => {
        setServerError(false);
        apiPostUser(myData).then(res => {
            if(res.status === 200) {
                history.push("/");
            } else {
                setServerError(true);
            }
        }).catch(err => {
            console.log(err);
        })
    }

    const apiPostUser = async (myData) => {
        let response = await fetch(`/users`, {
            method: 'POST', 
            body: JSON.stringify(myData),
            headers: { "Content-type" : "application/json; charset=UTF-8"} 
        });

        return response
    }

    return (
        <div className="box-layout">
            <Form submitFunction={createUser} serverError={serverError}/>
        </div>
    );
}

export default CreatePage