import React, { useEffect } from 'react';
import User from './user';
import { useSelector, useDispatch } from 'react-redux';
import { getUsers, logout } from '../redux/actions/actions';
import { Link } from 'react-router-dom';

const HomePage = () => {
  const users = useSelector(state => state.app.users);
  const dispatch = useDispatch();

  useEffect(() => {
    apiGetUsers().then(res => {
      dispatch(getUsers(res));
    }).catch(err => {
      console.log(err);
    });
  }, [])

  const apiGetUsers = async () => {
    let response = await fetch(`/users`);
    let body = await response.json();

    return body
  };

  return (
    <div className="content-container">
      <div className="list-header">
        <Link to={"/create"}>
          <button className="header-button">Create User</button>
        </Link>
        <button className="header-button" onClick={() => dispatch(logout())}>Sign Out</button>
      </div>
      <div className="list-body">
        {users.map(user => (
          <User key={user.id} user={user}/>
        ))}
      </div>
    </div>
  )
}

export default HomePage
